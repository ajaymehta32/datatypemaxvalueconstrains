package ajaymehta.datatypemaxvalueconstrains;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/5/2017.
 */

public class LongConstraint {

    static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.println(Long.MAX_VALUE);

        System.out.print("Enter long value: ");

            long x = sc.nextLong();  // 10^18  10 raise to power 18  //9,223,372,036,854,775,807  [19 digits] [ for positive n negative value]

        System.out.println(x);


    }
}
