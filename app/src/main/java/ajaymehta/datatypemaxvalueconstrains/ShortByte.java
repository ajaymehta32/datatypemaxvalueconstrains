    //1. What is the range of short data type in Java?
    public static void answer1(){

        System.out.println(Short.MAX_VALUE); // 10^4 // 5 digits
        System.out.println(Short.MIN_VALUE); // Short occupies 16 bits in memory. Its range is from -32768 to 32767.
    }

    //  2 What is the range of byte data type in Java?
    public static void answer2(){

        System.out.println(Byte.MAX_VALUE); // 10^2 // 3 digits..
        System.out.println(Byte.MIN_VALUE);  // Byte occupies 8 bits in memory. Its range is from -128 to 127.

    }


 public static void main(String args[]){

       answer1();
        answer2();

    }