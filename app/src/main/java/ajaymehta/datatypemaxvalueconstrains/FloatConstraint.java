package ajaymehta.datatypemaxvalueconstrains;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/6/2017.
 */

public class FloatConstraint {

    static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.println(Float.MAX_VALUE);

        System.out.print("Enter long value: ");

        float  x= sc.nextFloat(); //3.4028235E38  // it take 8 values in total.. before dot it takes max 7 VALUES  . if you add 7 before dot it will place only 1 digit after dot ..
        // if you place 1 digit before dot it can place max 7 digit after dot ..so it works in a more flexible way..

        System.out.println(x);


    }
}
