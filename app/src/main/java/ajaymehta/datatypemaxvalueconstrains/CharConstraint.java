package ajaymehta.datatypemaxvalueconstrains;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/6/2017.
 */

public class CharConstraint {

    static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.println(Character.MAX_VALUE); // it doesnt print..you dont see it ..in next line i am making it to print..
        System.out.println(Character.MAX_VALUE +0); // add zero ..if you add another integer it will subtract that much int value from char value..

        int x = (int) 'A';  // printing ASCII value of char -- space-32 , l-108 , } -125 , A - 65
        System.out.println(x);


    }

}
