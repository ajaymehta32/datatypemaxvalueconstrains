package ajaymehta.datatypemaxvalueconstrains;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/5/2017.
 */

// SEE  why i am making this because it will help you choose the data type accordingly given constraints in a competitive programming..like on CodeChef
    // whethen int or long will full fill the constrains condition



public class IntConstraint {

   static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.println(Integer.MAX_VALUE);  // or you can directly see value of it by access it constant from its class


        System.out.print("Enter int value: ");

     //   2,147,483,647 [10 digits max value for both positive and negative]  // see even if you increment a digit in this..it will trhough InputMisMatchException ex last three digits are..647 ..even if you make than 648 --> exception will be thrown

        int x = sc.nextInt(); // you can put comma inside values putting through keyboard (scanner) [ but it shoud contain 3 numbers after that put comma[

        //  2,147,483,647 [10 digits] -->>  int datatype full fill the constaint limit 10^9 (10 digits - 1000000000) means 9 zero's will be added after '1'


        System.out.println(x);



    }
}

// see how constrains are given
/*
Constraints
        1 ≤ T ≤ 106  // 10 raise to power 6  //  10^6 // non negative --starting from 1 (but if they are negative our int can go upto 10 negative numerbs too ..yeah!
        1 ≤ n ≤ 105  // 10 raise to power 5
        Sum of n over all the test cases ≤ 106
        -109 ≤ ai ≤ 109  // -10^9 to 10^9

*/

/*1
    Constraint Means the upper and lower limits of Input Data.It also means the Set of Valid Input Data.

        1 <= T <= 1000        //Means the value of T in Input Data set  is greater than or equal to 1
        //and less than or equal to 1000

        5 <= A[K] <= 100  //means the value of each member of Array A is greater than or equal to 5
        //and less than or equal to 100
        Code-chef is very serious ,they wont give u the data that is not allowed or that does not satisfy the constraint.Code-chef infact guarantee that there is no test data that does not satisfy the given constraint.

        " if user try to input data that is not allowed ask him again for data or it is just to let us see the limits of input"

        Well it never gives such data .You don't have to unnecessarily check and skip every-time while Taking Input.*/


/*

    Constraints are, as you said, the LIMITS (Upper and Lower) of the input data, and are VERY important when considering the solution of a problem. They give an idea of the data types to be used, approach to be adopted etc.

        Eg-

        Lets say input constraint of an integer n is 10^15. (10 raised to power 15)

        This means that-

        1)Using int to store the data will result in overflow (Here it hinted on data type to be used)

        2) If you think of creating an array of this size, you'd exceed memory limit of code and hence arrays cannot be used (here it ruled out any approach using arrays)

        3) With n this large (if its something we have to iterate over), a nested loop or anything using slower algo like O(n^2) will usually give TLE (exceed time limit. Since operations to be performed are a lot)

        And many more! You'd get the idea as you progress and practie!

        Hope it helped, happy coding! ^_^*/
